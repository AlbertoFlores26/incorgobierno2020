@extends('layouts.app')



@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h1>Información personal</h1></div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(session('flash'))
                    <div class="alert alert-success" role="alert">
                     <strong>Aviso</strong> {{session('flash')}}
                     <button type="button" class="Close" data-dismiss="alert" alert-label="Close"><span aria-hidden="true">&times;</span></button>
                     
                    </div>
                    @endif
                        <div class="form-row">
                          <div class="col-md-6 mb-3">
                            <label for="validationDefault01">Nombre</label>
                          <input type="text" class="form-control" id="validationDefault01" value="{{auth()->user()->name}}" required>
                          </div>
                          <div class="col-md-6 mb-3">
                            <label for="validationDefault02">Apellido Paterno</label>
                            <input type="text" class="form-control" id="validationDefault02" value="{{auth()->user()->apellidopat}}" required>
                          </div>
                        </div>

                        <div class="form-row">
                          <div class="col-md-6 mb-3">
                            <label for="validationDefault01">Apellido Materno</label>
                          <input type="text" class="form-control" id="validationDefault01" value="{{auth()->user()->apellidomat}}" required>
                          </div>
                          <div class="col-md-6 mb-3">
                            <label for="validationDefault02">Correo electrónico</label>
                            <input type="email" class="form-control" id="validationDefault02" value="{{auth()->user()->email}}" required>
                          </div>
                        </div>
                        <div class="form-row">
                          <div class="col-md-6 mb-3">
                            <label for="validationDefault01">CURP</label>
                          <input type="text" class="form-control" id="validationDefault01" value="{{auth()->user()->curp}}" required>
                          </div>
                          <div class="col-md-6 mb-3">
                            <label for="validationDefault02">Telefono</label>
                            <input type="tel" class="form-control" id="validationDefault02" value="{{auth()->user()->telefono}}" required>
                          </div>
                        </div>
                        
                        <div class="form-group">
                          <div class="form-check">
                            
                          </div>
                        </div>
                        <a class="btn btn-success btn-lg btn-block" href="{{ route('usuario.edit', auth()->id() ) }}">Editar</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
